import React, { useEffect, useState } from 'react';

function ConferenceForm() {
    const [locations, setLocations] = useState([]);

    const [formData, setFormData] = useState({
        name: '',
        starts: '',
        ends: '',
        description: '',
        max_presentations: '',
        max_attendees: '',
        location: '',
      });


    const handleSubmit = async (event) => {
        event.preventDefault();

        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setFormData({
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_presentations: '',
                max_attendees: '',
                location: '',
            });
        }
    }

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;

        setFormData({
          ...formData,
          [inputName]: value
        });
      }



    const fetchData = async () => {
        const url = "http://localhost:8000/api/locations/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
    <div className="container">
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input value={formData.name} onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.starts} onChange={handleFormChange} placeholder="mm/dd/yyyy" required type="date" name="starts" id="starts" className="form-control" />
                            <label htmlFor="starts">Starts</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.ends} onChange={handleFormChange} placeholder="mm/dd/yyyy" required type="date" name="ends" id="ends" className="form-control" />
                            <label htmlFor="ends">Ends</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="description">Description</label>
                            <textarea value={formData.description} onChange={handleFormChange} className="form-control" name="description" id="description" rows="3" required></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.max_presentations} onChange={handleFormChange} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
                            <label htmlFor="max_presentations">Maximum presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.max_attendees} onChange={handleFormChange} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
                            <label htmlFor="max_attendees">Maximum attendees</label>
                        </div>
                        <div className="mb-3">
                            <select value={formData.location} onChange={handleFormChange} required id="location" name="location" className="form-select">
                            <option value="">Choose a location</option>
                            {locations.map(location => {
                                return (
                                    <option key={location.id} value={location.id}>
                                        {location.name}
                                    </option>
                                );
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    )
}

export default ConferenceForm;
